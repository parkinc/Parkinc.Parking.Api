package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

var redisClient *redis.Client

type ParkingData struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Max  int    `json:"maxCount"`
	Free int    `json:"freeCount"`
}

func handleGetData(w http.ResponseWriter, r *http.Request) {
	logrus.Info("Retrieving parking data from redis")
	dataJSON, err := redisClient.Get("ParkingPlaces").Result()
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	var data []ParkingData
	err = json.Unmarshal([]byte(dataJSON), &data)
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	responseJSON, err := json.Marshal(data)
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	logrus.Info(w.Header())
	w.Write(responseJSON)
}

func main() {

	redisURL := os.Getenv("REDIS_URL")

	logrus.Info("Starting Redis client at ", redisURL)
	redisClient = redis.NewClient(&redis.Options{
		Addr:     redisURL,
		Password: "",
		DB:       0,
	})

	pong, err := redisClient.Ping().Result()
	fmt.Println(pong, err)

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/data", handleGetData)
	logrus.Fatal(http.ListenAndServe(":8080", router))
	logrus.Info("Server running at localhost:8080")
}
